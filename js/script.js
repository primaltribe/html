$( document ).ready(function() {

//    Open Menu
$('.header-menu').click(function(){
    //$('.navbar').css('display', 'block');
    $('.navbar').css('height', $(document).height());
    $('.navbar').css('display', 'block').animateCss('fadeInRight');
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $('.header-menu').addClass('animated fadeOut').one(animationEnd, function() {
        $('.header-menu').removeClass('animated fadeOut').css('display', 'none');
    });
    //$('.header-menu').animateCss('fadeOut').css('display', 'none');
});

//    Close Menu
$('.close-btn').click(function(){
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $('.navbar').addClass('animated fadeOutRight').one(animationEnd, function() {
        $('.navbar').removeClass('animated fadeOutRight').css('display', 'none').removeAttr('style');
    });

    $('.header-menu').css('display', 'block').addClass('animated fadeIn').one(animationEnd, function() {
        $('.header-menu').removeAttr('style').removeClass('animated fadeIn');
    });

});

//    Animate the top buttons
$('.largebutton').mouseover(function(){
    $(this).find('.fa-8').animateCss('bounce');
});

//    Animate the footer buttons
$('footer a').mouseover(function() {
    $(this).animateCss('tada');
});



    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            $(this).addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName);
            });
        }
    });

});

